package modelo;

import java.io.Serializable;
import javax.persistence.*;

@Entity
public class Aluno implements Serializable{
    
    @Id
    private int id;
    
    private String nome;
    
    @Column(unique = true, length = 11)
    private String numeroMatricula;

    public Aluno() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumeroMatricula() {
        return numeroMatricula;
    }

    public void setNumeroMatricula(String numeroMatricula) {
        this.numeroMatricula = numeroMatricula;
    }
    
}
