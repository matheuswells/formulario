package dao;
import javax.persistence.*;
import modelo.Aluno;

public class AlunoDAO {
   
   EntityManagerFactory fabrica;
   EntityManager operacao;
    
   public AlunoDAO(){
    fabrica  = Persistence.createEntityManagerFactory("EscolaPU");   
    operacao = fabrica.createEntityManager();
   }
   public void salvar(Aluno aluno){
       operacao.getTransaction().begin();
       operacao.persist(aluno);
       operacao.getTransaction().commit();
   }
   
   public void fecharSessao(){
       operacao.close();
       fabrica.close();
   }
   

  
    
}
